
# Pont Systems Teszt Alkalmazás
Az alkalmazást fejlesztette: [Kónya Tamás](mailto:konyatamas01@gmail.com)

Működő éles verzió: [https://pont-systems-test.innocode.hu](https://pont-systems-test.innocode.hu)

**Összegzés:** A React-hoz Typescript nyelvet használtam, úgy gondolom ez később hasznos lehet, amikor be kell kötni az API-t. A kód refaktorálva van, került bele Redux Store, React Router, MaterialUI, mockapi.io-val megvalósított API hívás saját ApiService-en. Persistent State szolgálja a böngészőben tárolt UI beállításokat, pl.: sidebar állapota. A Header komponensre került egy theme switcher, prezentálva a színek változtatását. Minden adat Redux-on megy keresztül. Úgy gondolom az alkalmazás megfelel minden kritériumnak és remélem elnyeri majd a tetszést :) .


## Development scriptek

### `yarn start`

App futtatása development módban: [http://localhost:3000](http://localhost:3000) link megnyitása a böngészőben.

### `yarn build`

Build-eli az appot a `build` mappába.

A lefordított fájlok feltölthetők egy egyszerű Apache szerverre.

További infó: [Deployment](https://facebook.github.io/create-react-app/docs/deployment)

## Egyéb info

Felhasznált framework, programozási nyelv [React JS + Redux + Typescript](https://facebook.github.io/create-react-app/docs/getting-started).

