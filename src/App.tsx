import React, {useEffect, useState} from 'react';
import { useSelector } from 'react-redux';
import { MuiThemeProvider, createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

import { mainTheme, mainThemeDark} from 'themes';
import { Layout } from 'components/Layout';
import { selectTheme } from 'store/ui/uiSlice';

function App() {
  const themeValue = useSelector(selectTheme);
  const [theme, setTheme] = useState(themeValue === 'dark' ? mainThemeDark : mainTheme);
  useEffect(() => {
    setTheme(themeValue === 'dark' ? mainThemeDark : mainTheme);
  }, [themeValue]);
  return (
    <MuiThemeProvider theme={responsiveFontSizes(createMuiTheme(theme))}>
      <Layout />
    </MuiThemeProvider>
  );
}

export default App;
