const BASE_API_URL = 'https://5ebdb13dec34e900161924c0.mockapi.io/api/v1';

const TABS = [
  {
    index: 'one',
    title: 'Alapvető ügyféladatok'
  },
  {
    index: 'two',
    title: 'Kapcsolatok',
  },
  {
    index: 'three',
    title: 'Tulajdonosok/Vezető tisztségviselők',
  },
  {
    index: 'four',
    title: 'Kapcsolt vállalkozások',
  },
  {
    index: 'five',
    title: 'További biztosítéknyújtók',
  },
];

const TELEPHELYEK = [
  {
    id: 1,
    country: 'Magyarország',
    postcode: '5700',
    city: 'Gyula',
    street: 'Temesvári',
    street_type: 'út',
    house_number: '5/1',
  },
  {
    id: 2,
    country: 'Románia',
    postcode: '12345',
    city: 'Arad',
    street: 'Valami',
    street_type: 'út',
    house_number: '120',
  },
];

const SIDEBAR_MENU_ITEMS = [
  {
    id: 1,
    title: 'Ügyfél alapadatok',
    submenu: [
      {
        id: 1,
        title: 'Hozzáférők',
        url: '/accesses',
      },
      {
        id: 2,
        title: 'Hivatalos alapadat',
        url: '/client-profile',
      },
      {
        id: 3,
        title: 'Szerkesztés alatt álló alapadat',
        url: '/szerkesztes-alatt',
      },
      {
        id: 4,
        title: 'Korábbi alapadat változatok',
        url: '/korabbi-alapadat-valtozatok',
      },
      {
        id: 5,
        title: 'Csoporttagság történet',
        url: '/csoporttagsag-tortenet',
      },
    ],
  },
  {
    id: 2,
    title: 'Adminisztrátori menü',
    submenu: [
      {
        id: 1,
        title: 'Tooltip és HELP szövegek karbantartása',
        url: '/tooltip-helps',
      },
      {
        id: 2,
        title: 'Email üzenetek paraméterezése',
        url: '/email-parameters',
      },
      {
        id: 3,
        title: 'Más rendszerre mutató linkek karbantartása',
        url: '/external-links',
      },
      {
        id: 4,
        title: 'Jobok',
        url: '/jobs',
      },
      {
        id: 5,
        title: 'Rendszerparaméter',
        url: '/system-parameter',
      },
    ],
  },
];

export {BASE_API_URL, TABS, TELEPHELYEK, SIDEBAR_MENU_ITEMS};
