import ClientProfile from '../containers/ClientProfile';
import ToolTipHelps from '../containers/ToolTipHelps';

type typeRoute = {
  path: string;
  exact?: boolean;
  redirectTo?: string;
  component?: any;
};

export const routes: typeRoute[] = [
  {
    path: '/',
    exact: true,
    redirectTo: "/client-profile",
  },
  {
    path: "/client-profile",
    exact: true,
    component: ClientProfile,
  },
  {
    path: "/tooltip-helps",
    exact: true,
    component: ToolTipHelps,
  }
];
