export const countries = [
  {
    "country": "Afganisztán"
  },
  {
    "country": "Aland"
  },
  {
    "country": "Albánia"
  },
  {
    "country": "Algéria"
  },
  {
    "country": "Amerikai Egyesült Államok"
  },
  {
    "country": "Amerikai Szamoa"
  },
  {
    "country": "Amerikai Virgin-szigetek"
  },
  {
    "country": "Andorra"
  },
  {
    "country": "Angola"
  },
  {
    "country": "Anguilla"
  },
  {
    "country": "Anktartisz"
  },
  {
    "country": "Antigua és Barbuda"
  },
  {
    "country": "Argentina"
  },
  {
    "country": "Aruba"
  },
  {
    "country": "Ausztrália"
  },
  {
    "country": "Ausztria"
  },
  {
    "country": "Az Amerikai Egyesült Államok lakatlan külbirtokai"
  },
  {
    "country": "Azerbajdzsán"
  },
  {
    "country": "Bahama-szigetek"
  },
  {
    "country": "Bahrein"
  },
  {
    "country": "Banglades"
  },
  {
    "country": "Barbados"
  },
  {
    "country": "Belgium"
  },
  {
    "country": "Belize"
  },
  {
    "country": "Belorusszia"
  },
  {
    "country": "Benin"
  },
  {
    "country": "Bermuda"
  },
  {
    "country": "Bhután"
  },
  {
    "country": "Bissau-Guinea"
  },
  {
    "country": "Bolívia"
  },
  {
    "country": "Bonaire, Sint Eustatius és Saba"
  },
  {
    "country": "Bosznia-Hercegovina"
  },
  {
    "country": "Botswana"
  },
  {
    "country": "Bouvet-sziget"
  },
  {
    "country": "Brazília"
  },
  {
    "country": "Brit Indiai-óceáni Terület"
  },
  {
    "country": "Brit Virgin-szigetek"
  },
  {
    "country": "Brunei"
  },
  {
    "country": "Bulgária"
  },
  {
    "country": "Burkina Faso"
  },
  {
    "country": "Burundi"
  },
  {
    "country": "Chile"
  },
  {
    "country": "Ciprus"
  },
  {
    "country": "Comore-szigetek"
  },
  {
    "country": "Cook-szigetek"
  },
  {
    "country": "Costa Rica"
  },
  {
    "country": "Csád"
  },
  {
    "country": "Csehország"
  },
  {
    "country": "Curaçao"
  },
  {
    "country": "Dánia"
  },
  {
    "country": "Dél-afrikai Köztársaság"
  },
  {
    "country": "Déli-Georgia és Déli-Sandwich-szigetek"
  },
  {
    "country": "Dél-Korea"
  },
  {
    "country": "Dél-Szudán"
  },
  {
    "country": "Dominika"
  },
  {
    "country": "Dominikai Köztársaság"
  },
  {
    "country": "Dzsibuti"
  },
  {
    "country": "Ecuador"
  },
  {
    "country": "Egyenlítői Guinea"
  },
  {
    "country": "Egyesült Arab Emírségek"
  },
  {
    "country": "Egyiptom"
  },
  {
    "country": "Elefántcsontpart"
  },
  {
    "country": "Eritrea"
  },
  {
    "country": "Északi-Mariana-szigetek"
  },
  {
    "country": "Észak-Korea"
  },
  {
    "country": "Észtország"
  },
  {
    "country": "Etiópia"
  },
  {
    "country": "Falkland-szigetek"
  },
  {
    "country": "Feröer"
  },
  {
    "country": "Fidzsi-szigetek"
  },
  {
    "country": "Finnország"
  },
  {
    "country": "Francia déli és antarktiszi területek"
  },
  {
    "country": "Francia Guyana"
  },
  {
    "country": "Francia Polinézia"
  },
  {
    "country": "Franciaország"
  },
  {
    "country": "Fülöp-szigetek"
  },
  {
    "country": "Gabon"
  },
  {
    "country": "Gambia"
  },
  {
    "country": "Ghána"
  },
  {
    "country": "Gibraltár"
  },
  {
    "country": "Görögország"
  },
  {
    "country": "Grenada"
  },
  {
    "country": "Grönland"
  },
  {
    "country": "Grúzia"
  },
  {
    "country": "Guadeloupe"
  },
  {
    "country": "Guam"
  },
  {
    "country": "Guatemala"
  },
  {
    "country": "Guernsey"
  },
  {
    "country": "Guinea"
  },
  {
    "country": "Guyana"
  },
  {
    "country": "Haiti"
  },
  {
    "country": "Heard-sziget és McDonald-szigetek"
  },
  {
    "country": "Hollandia"
  },
  {
    "country": "Honduras"
  },
  {
    "country": "Hong Kong"
  },
  {
    "country": "Horvátország"
  },
  {
    "country": "India"
  },
  {
    "country": "Indonézia"
  },
  {
    "country": "Irak"
  },
  {
    "country": "Irán"
  },
  {
    "country": "Írország"
  },
  {
    "country": "Ismeretlen"
  },
  {
    "country": "Izland"
  },
  {
    "country": "Izrael"
  },
  {
    "country": "Jamaica"
  },
  {
    "country": "Japán"
  },
  {
    "country": "Jemen"
  },
  {
    "country": "Jersey"
  },
  {
    "country": "Jordánia"
  },
  {
    "country": "Kajmán-szigetek"
  },
  {
    "country": "Kambodzsa"
  },
  {
    "country": "Kamerun"
  },
  {
    "country": "Kanada"
  },
  {
    "country": "Karácsony-sziget"
  },
  {
    "country": "Katar"
  },
  {
    "country": "Kazahsztán"
  },
  {
    "country": "Kelet-Timor"
  },
  {
    "country": "Kenya"
  },
  {
    "country": "Kína"
  },
  {
    "country": "Kínai Köztársaság (Tajvan)"
  },
  {
    "country": "Kirgizisztán"
  },
  {
    "country": "Kiribati"
  },
  {
    "country": "Kókusz-sziget"
  },
  {
    "country": "Kolumbia"
  },
  {
    "country": "Kongó"
  },
  {
    "country": "Kongói Demokratikus Köztársaság"
  },
  {
    "country": "Koszovó"
  },
  {
    "country": "Közép-afrikai Köztársaság"
  },
  {
    "country": "Kuba"
  },
  {
    "country": "Kuvait"
  },
  {
    "country": "Laosz"
  },
  {
    "country": "Lengyelország"
  },
  {
    "country": "Lesotho"
  },
  {
    "country": "Lettország"
  },
  {
    "country": "Libanon"
  },
  {
    "country": "Libéria"
  },
  {
    "country": "Líbia"
  },
  {
    "country": "Liechtenstein"
  },
  {
    "country": "Litvánia"
  },
  {
    "country": "Luxemburg"
  },
  {
    "country": "Macao"
  },
  {
    "country": "Macedónia"
  },
  {
    "country": "Madagaszkár"
  },
  {
    "country": "Magyarország"
  },
  {
    "country": "Malajzia"
  },
  {
    "country": "Malawi"
  },
  {
    "country": "Maldiv-szigetek"
  },
  {
    "country": "Mali"
  },
  {
    "country": "Málta"
  },
  {
    "country": "Man-sziget"
  },
  {
    "country": "Marokkó"
  },
  {
    "country": "Marshall-szigetek"
  },
  {
    "country": "Martinique"
  },
  {
    "country": "Mauritánia"
  },
  {
    "country": "Mauritius"
  },
  {
    "country": "Mayotte"
  },
  {
    "country": "Mexikó"
  },
  {
    "country": "Mianmar"
  },
  {
    "country": "Mikronézia"
  },
  {
    "country": "Moldova"
  },
  {
    "country": "Monaco"
  },
  {
    "country": "Mongólia"
  },
  {
    "country": "Montenegro"
  },
  {
    "country": "Montserrat"
  },
  {
    "country": "Mozambik"
  },
  {
    "country": "Nagy-Britannia"
  },
  {
    "country": "Namíbia"
  },
  {
    "country": "Nauru"
  },
  {
    "country": "Németország"
  },
  {
    "country": "Nepál"
  },
  {
    "country": "Nicaragua"
  },
  {
    "country": "Niger"
  },
  {
    "country": "Nigéria"
  },
  {
    "country": "Niue"
  },
  {
    "country": "Norfolk-sziget"
  },
  {
    "country": "Norvégia"
  },
  {
    "country": "Nyugat-Szahara"
  },
  {
    "country": "Olaszország"
  },
  {
    "country": "Oman"
  },
  {
    "country": "Oroszország"
  },
  {
    "country": "Örményország"
  },
  {
    "country": "Pakisztán"
  },
  {
    "country": "Palau"
  },
  {
    "country": "Palesztína"
  },
  {
    "country": "Panama"
  },
  {
    "country": "Pápua Új-Guinea"
  },
  {
    "country": "Paraguay"
  },
  {
    "country": "Peru"
  },
  {
    "country": "Pitcairn-szigetek"
  },
  {
    "country": "Portugália"
  },
  {
    "country": "Puerto Rico"
  },
  {
    "country": "Réunion"
  },
  {
    "country": "Románia"
  },
  {
    "country": "Ruanda"
  },
  {
    "country": "Saint Kitts és Nevis"
  },
  {
    "country": "Saint Vincent és a Grenadine-szigetek"
  },
  {
    "country": "Saint-Barthélemy"
  },
  {
    "country": "Saint-Pierre és Miquelon"
  },
  {
    "country": "Salamon-szigetek"
  },
  {
    "country": "Salvador"
  },
  {
    "country": "San Marino"
  },
  {
    "country": "Sao Tomé"
  },
  {
    "country": "Seychelle-szigetek"
  },
  {
    "country": "Sierra Leone"
  },
  {
    "country": "Spanyolország"
  },
  {
    "country": "Sri Lanka"
  },
  {
    "country": "Suriname"
  },
  {
    "country": "Svájc"
  },
  {
    "country": "Svalbard and Jan Mayen"
  },
  {
    "country": "Svédország"
  },
  {
    "country": "Szamoa"
  },
  {
    "country": "Szaud-Arábia"
  },
  {
    "country": "Szenegál"
  },
  {
    "country": "Szent Ilona, Ascension és Tristan da Cunha"
  },
  {
    "country": "Szent Lucia"
  },
  {
    "country": "Szent Márton-sziget (Francia rész)"
  },
  {
    "country": "Szent Márton-sziget (Holland rész)"
  },
  {
    "country": "Szerbia"
  },
  {
    "country": "Szingapúr"
  },
  {
    "country": "Szíria"
  },
  {
    "country": "Szlovákia"
  },
  {
    "country": "Szlovénia"
  },
  {
    "country": "Szomália"
  },
  {
    "country": "Szudán"
  },
  {
    "country": "Szváziföld"
  },
  {
    "country": "Tadzsikisztán"
  },
  {
    "country": "Tanzánia"
  },
  {
    "country": "Thaiföld"
  },
  {
    "country": "Togo"
  },
  {
    "country": "Tokelau-szigetek"
  },
  {
    "country": "Tonga"
  },
  {
    "country": "Törökország"
  },
  {
    "country": "Trinidad és Tobago"
  },
  {
    "country": "Tunézia"
  },
  {
    "country": "Turks- és Caicos-szigetek"
  },
  {
    "country": "Tuvalu"
  },
  {
    "country": "Türkmenisztán"
  },
  {
    "country": "Uganda"
  },
  {
    "country": "Új-Kaledónia"
  },
  {
    "country": "Új-Zéland"
  },
  {
    "country": "Ukrajna"
  },
  {
    "country": "Uruguay"
  },
  {
    "country": "Üzbegisztán"
  },
  {
    "country": "Vanuatu"
  },
  {
    "country": "Vatikán"
  },
  {
    "country": "Venezuela"
  },
  {
    "country": "Vietnam"
  },
  {
    "country": "Wallis és Futuna"
  },
  {
    "country": "Zambia"
  },
  {
    "country": "Zimbabwe"
  },
  {
    "country": "Zöld-foki Köztársaság"
  }
];
