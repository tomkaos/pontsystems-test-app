export const TOOLTIP_API_MOCK_RESPONSE = {
  "content": [
    {
      "id": "clientform",
      "urlapNev": "Ügyfél űrlap",
      "tab": null,
      "blokk": null,
      "mezo": null,
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": null,
      "mezo": null,
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.bankmasterek",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Bankmasterek",
      "mezo": null,
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.bankmasterek.bankmasterAzonositok.bankmasterAzonositok.bmAzonosito",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Bankmasterek",
      "mezo": "Bankmaster / Bankmaster azonosító",
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.bankmasterek.bankmasterAzonositok.bankmasterAzonositok.bmAzonositoNyitasDatum",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Bankmasterek",
      "mezo": "Bankmaster / Nyitás dátuma",
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.lakcim",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Lakcím",
      "mezo": null,
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.lakcim.lakcimHazszam",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Lakcím",
      "mezo": "Házszám",
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.lakcim.lakcimIrsz",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Lakcím",
      "mezo": "Irányítószám",
      "tooltip": "Irányítószám",
      "sugo": "Irányítószám"
    },
    {
      "id": "clientform.alap_azonosito_blokk.lakcim.lakcimIrszKulfoldi",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Lakcím",
      "mezo": "Külföldi irányítószám",
      "tooltip": null,
      "sugo": null
    },
    {
      "id": "clientform.alap_azonosito_blokk.lakcim.lakcimKtJelleg",
      "urlapNev": "Ügyfél űrlap",
      "tab": "Alapvető ügyféladatok",
      "blokk": "Lakcím",
      "mezo": "Közterület jellege",
      "tooltip": null,
      "sugo": null
    }
  ],
  "pageable": {
    "sort": {
      "unsorted": false,
      "sorted": true,
      "empty": false
    },
    "pageNumber": 0,
    "offset": 0,
    "pageSize": 10,
    "paged": true,
    "unpaged": false
  },
  "last": false,
  "totalPages": 21,
  "totalElements": 202,
  "sort": {
    "unsorted": false,
    "sorted": true,
    "empty": false
  },
  "numberOfElements": 10,
  "number": 0,
  "size": 10,
  "first": true,
  "empty": false
};
