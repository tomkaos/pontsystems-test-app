const mainTheme = {
  palette: {
    common: {black: "#000", white: "#fff"},
    background: {paper: "#fff", default: "#fafafa"},
    primary: {light: "#ff7a73", main: "#f54b42", dark: "#f53127", contrastText: "#fff"},
    secondary: {light: "#6091e0", main: "#3074e3", dark: "#0958d9", contrastText: "#fff"},
    error: {light: "#e57373", main: "#f44336", dark: "#d32f2f", contrastText: "#fff"},
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  },
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: '#424242',
        color: '#fff',
      }
    }
  }
};

const mainThemeDark = {
  ...mainTheme,
  palette: {
    ...mainTheme.palette,
    background: {paper: "#333", default: "#222"},
    primary: {light: "#7986cb", main: "#333", dark: "#121212", contrastText: "#fff"},
    text: {
      primary: "#fff",
      secondary: "#fff",
      disabled: "#fff",
      hint: "#fff"
    },
    type: 'dark',
  },
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: '#090909',
        color: '#fff',
      }
    }
  },
};

export {mainTheme, mainThemeDark};
