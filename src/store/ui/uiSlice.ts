import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {AppThunk, RootState} from 'store/store';
import {Api} from 'services/api';

type uiState = {
  theme: string;
  sideBarOpen: boolean;
  advert: string;
  advertLoading: boolean;
};

const initialState: uiState = {
  theme: 'default',
  sideBarOpen: true,
  advert: '',
  advertLoading: false,
};

export const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    changeTheme: (state, action: PayloadAction<string>) => {
      state.theme = action.payload;
    },
    toggleSideBar: (state) => {
      state.sideBarOpen = !state.sideBarOpen;
    },
    setAdvertLoading: (state, action: PayloadAction<boolean>) => {
      state.advertLoading = action.payload;
    },
    setAdvert: (state, action: PayloadAction<string>) => {
      state.advert = action.payload;
    },
  }
});

export const { changeTheme, toggleSideBar, setAdvert, setAdvertLoading } = uiSlice.actions;

export const getAdvert = (): AppThunk => dispatch => {
  Api.get('/advert').then(response => {
    dispatch(setAdvertLoading(true));
    const {data} = response;
    //@ts-ignore
    dispatch(setAdvert(data.advertText));
    dispatch(setAdvertLoading(false));
  })
}

export const selectTheme = (state: RootState) => state.ui.theme;
export const selectSideBar = (state: RootState) => state.ui.sideBarOpen;
export const selectRouter = (state: RootState) => state.router;
export const selectAdvert = (state: RootState) => state.ui.advert;
export const selectAdvertLoading = (state: RootState) => state.ui.advertLoading;

export default uiSlice.reducer;
