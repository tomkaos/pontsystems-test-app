import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {AppThunk, RootState} from 'store/store';
import {Api} from 'services/api';
import {TOOLTIP_API_MOCK_RESPONSE} from 'config/tooltipApiResponse';

export type tooltipElement = {
  id: string | null;
  urlapNev: string | null;
  tab: string | null;
  blokk: string | null;
  mezo: string | null;
  tooltip: string | null;
  sugo: string | null;
};

export type tooltipResponse = {
  content: tooltipElement[],
  pageable: {
    sort: {
      unsorted: boolean,
      sorted: boolean,
      empty: boolean,
    },
    pageNumber: number,
    offset: number,
    pageSize: number,
    paged: boolean,
    unpaged: boolean,
  },
  last?: boolean,
  totalPages: number,
  totalElements: number,
  sort?: {
    unsorted: boolean,
    sorted: boolean,
    empty: boolean,
  },
  numberOfElements?: number,
  number?: number,
  size?: number,
  first?: boolean,
  empty?: boolean,
};

type RequestFilter = {
  name: string,
  value: string,
  operation: string,
};

type RequestOrder = {
  property: string,
  direction: string,
}

type tooltipsRequest = {
  filters: RequestFilter[],
  orders: RequestOrder[],
  size: number,
  page: number,
};

type adminTooltipsState = {
  loading: boolean,
  data: tooltipResponse,
  filter: tooltipsRequest,
};

export type updateFilterPayload = {
  filterId: string,
  value: string,
};

const initialState: adminTooltipsState = {
  loading: false,
  data: {
    content: [],
    totalElements: 0,
    totalPages: 0,
    pageable: {
      sort: {
        unsorted: false,
        sorted: false,
        empty: true,
      },
      pageNumber: 1,
      offset: 0,
      pageSize: 10,
      paged: false,
      unpaged: false,
    },
  },
  filter: {
    size: 10,
    page: 0,
    filters: [
      {
        name: 'urlapNev',
        value: 'Ügyfél',
        operation: 'CASE_INSENSITIVE_LIKE',
      },
    ],
    orders: [
      {
        property: 'id',
        direction: 'ASC',
      },
    ],
  },
};

export const adminTooltipsSlice = createSlice({
  name: 'adminTooltips',
  initialState,
  reducers: {
    setTooltipsLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    updateTooltipsData: (state, action: PayloadAction<tooltipResponse>) => {
      state.data = action.payload;
    },
    updatePage: (state, action: PayloadAction<number>) => {
      state.filter = {
        ...state.filter,
        page: action.payload,
      };
    },
    updatePageSize: (state, action: PayloadAction<number>) => {
      state.filter = {
        ...state.filter,
        size: action.payload,
      };
    },
    updateFilter: (state, action: PayloadAction<updateFilterPayload>) => {
      const {filterId, value} = action.payload;
      const foundIndex = state.filter.filters.findIndex(element => element.name === filterId);
      if (foundIndex > -1) {
        if (value !== '') {
          state.filter.filters[foundIndex] = {
            ...state.filter.filters[foundIndex],
            value,
          };
        } else {
          const filteredArray = state.filter.filters.filter((item, index) => index !== foundIndex);
          state.filter.filters = filteredArray;
        }
      } else {
        state.filter.filters.push({
          name: filterId,
          value,
          operation: 'CASE_INSENSITIVE_LIKE',
        });
      }
    }
  },
});

export const {
  setTooltipsLoading,
  updateTooltipsData,
  updatePage,
  updatePageSize,
  updateFilter,
} = adminTooltipsSlice.actions;

export const getTooltipsData = (filter: tooltipsRequest): AppThunk => dispatch => {
  dispatch(setTooltipsLoading(true));
  Api.post('/admin-tooltips', filter).then(response => {
    setTimeout(() => {
      const {data} = response;
      // @ts-ignore
      dispatch(updateTooltipsData(data));
      dispatch(setTooltipsLoading(false));
    }, 1300);
  }).catch(() => {
    //Exceeded mock api limit, will load MOCK data
    setTimeout(() => {
      const data = TOOLTIP_API_MOCK_RESPONSE;
      // @ts-ignore
      dispatch(updateTooltipsData(data));
      dispatch(setTooltipsLoading(false));
    }, 1300);
  });
};

export const selectTooltips = (state: RootState) => state.adminTooltips;

export default adminTooltipsSlice.reducer;
