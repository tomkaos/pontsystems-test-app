import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {RootState} from 'store/store';

import {TELEPHELYEK} from 'config/constants';

export type addressType = {
  id: number,
  country: string;
  postcode: string;
  city: string;
  street: string;
  street_type: string;
  house_number: string;
};

type clientState = {
  addresses: addressType[],
};

type updateAddressPayload = {
  id: number,
  name: string,
  value: string,
};

const initialState: clientState = {
  addresses: TELEPHELYEK
};

export const clientSlice = createSlice({
  name: 'client',
  initialState,
  reducers: {
    updateAddress: (state, action: PayloadAction<updateAddressPayload>) => {
      const {id, name, value} = action.payload;
      const foundIndex = state.addresses.findIndex(x => x.id === id);
      state.addresses[foundIndex] = {
        ...state.addresses[foundIndex],
        [name]: value,
      };
    },
  },
});

export const { updateAddress } = clientSlice.actions;
export const selectAddresses = (state: RootState) => state.client.addresses;

export default clientSlice.reducer;
