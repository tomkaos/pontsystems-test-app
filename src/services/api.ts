import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

import {BASE_API_URL} from 'config/constants';

class ApiService {
  [x:string]: any;

  public constructor (config?: AxiosRequestConfig) {
    this.api = axios.create(config);

    this.api.interceptors.request.use((param: AxiosRequestConfig) => ({
      baseUrl: BASE_API_URL,
      ...param
    }));

    this.get = this.get.bind(this);
    this.delete = this.delete.bind(this);
    this.post = this.post.bind(this);
    this.put = this.put.bind(this);
  }

  public get<T, R = AxiosResponse<T>> (url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.api.get(url, config);
  }

  public delete<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.api.delete(url, config);
  }

  public post<T, B, R = AxiosResponse<T>> (url: string, data?: B, config?: AxiosRequestConfig): Promise<R> {
    return this.api.post(url, data, config);
  }

  public put<T, B, R = AxiosResponse<T>>(url: string, data?: B, config?: AxiosRequestConfig): Promise<R> {
    return this.api.put(url, data, config);
  }
}

export const Api = new ApiService({
  baseURL: BASE_API_URL,
});
