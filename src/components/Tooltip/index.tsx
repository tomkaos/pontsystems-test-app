import React, {ReactElement} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

type Props = {
  content: ReactElement | string,
  children: ReactElement,
}

export default function CustomTooltip({content, children}: Props) {
  return (
      <HtmlTooltip title={content} placement="top">
        {children}
      </HtmlTooltip>
  );
}
