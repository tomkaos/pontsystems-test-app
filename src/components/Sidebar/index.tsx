import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {
  Drawer,
  Toolbar,
  Divider,
  Box,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
} from '@material-ui/core';
import ChevronRight from '@material-ui/icons/ChevronRight';
import ExpandLess from '@material-ui/icons/ExpandLess';

import {selectSideBar} from 'store/ui/uiSlice';
import SubListItem from './SubListItem';
import {SIDEBAR_MENU_ITEMS} from 'config/constants';
import {useStyles} from './styles';

export const drawerWidth = 240;

function Sidebar() {
  const classes = useStyles();
  const [visibleMenu, setVisibleMenu] = useState(0);
  const sideBarOpen = useSelector(selectSideBar);
  const history = useHistory();

  useEffect(() => {
    let menuToExpand = 0;
    const currentUrl = history.location.pathname;
    SIDEBAR_MENU_ITEMS.forEach(mainMenuItem => {
      mainMenuItem.submenu.forEach(subMenuItem => {
        if (subMenuItem.url === currentUrl) {
          menuToExpand = mainMenuItem.id;
        }
      });
    });
    setVisibleMenu(menuToExpand);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggleMenu = (id: number) => {
    if (visibleMenu === id) {
      setVisibleMenu(0);
    } else {
      setVisibleMenu(id);
    }
  }

  return (
      <Drawer
          className={classes.drawer}
          variant="persistent"
          open={sideBarOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <Box className={classes.sideBarTop}>
            <Typography variant="caption">Aktuális ügyfél neve:</Typography>
            <Typography variant="subtitle1">Teszt Civilszervezet</Typography>
            <Typography variant="caption" color="error">
              Figyelem! Az ügyfél csődeljárás alatt.
            </Typography>
          </Box>
          <List component="nav">
            <Divider className={classes.divider} />

            {SIDEBAR_MENU_ITEMS.map(mainMenuItem => {
              return (
                <React.Fragment key={mainMenuItem.id}>
                  <ListItem button onClick={() => toggleMenu(mainMenuItem.id)}>
                    <ListItemIcon className={classes.listItemIcon}>
                      {visibleMenu === mainMenuItem.id ? <ExpandLess /> : <ChevronRight />}
                    </ListItemIcon>
                    <ListItemText primary={mainMenuItem.title} />
                  </ListItem>
                  <Collapse in={visibleMenu === mainMenuItem.id} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {mainMenuItem.submenu.map(subMenuItem => <SubListItem
                          url={subMenuItem.url}
                          title={subMenuItem.title}
                          key={subMenuItem.id}
                      />)}
                    </List>
                  </Collapse>
                  <Divider className={classes.divider} />
                </React.Fragment>
              )
            })}
          </List>
        </div>
      </Drawer>
  );
}

export default Sidebar;
