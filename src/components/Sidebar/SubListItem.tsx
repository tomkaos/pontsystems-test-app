import React from 'react';
import {useRouteMatch} from 'react-router';
import {Link} from 'react-router-dom';
import {ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import SubItemIcon from '@material-ui/icons/SubdirectoryArrowRight';
import ActiveSubItemIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: theme.spacing(4),
  },
  listItemIcon: {
    minWidth: 'auto',
    color: '#fff',
  },
  icon: {
    fontSize: 14,
    marginRight: 10,
  },
  activeIcon: {
    color: theme.palette.success.main,
    fontSize: 16,
    marginRight: 8,
  },
  activeText: {
    fontWeight: 'bold',
  }
}));

type Props = {
  url: string;
  title: string;
};

function SubListItem({url, title}: Props) {
  const classes = useStyles();
  const match = useRouteMatch(url);
  const active = !!(match && match.url === match.path);
  return (
    <ListItem
        button
        className={classes.nested}
        to={url}
        component={Link}
    >
      <ListItemIcon className={classes.listItemIcon}>
        {!active ?
            <SubItemIcon className={classes.icon} /> :
            <ActiveSubItemIcon className={classes.activeIcon} />
        }
      </ListItemIcon>
      <ListItemText disableTypography primary={title} className={active ? classes.activeText : undefined}/>
    </ListItem>
  );
}

export default SubListItem;
