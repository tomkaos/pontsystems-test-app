import {makeStyles} from '@material-ui/core/styles';
import {drawerWidth} from './index';

export const useStyles = makeStyles(() => ({
  divider: {
    backgroundColor: 'rgba(255, 255, 255, 0.12)',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  sideBarTop: {
    padding: 10,
  },
  listItemIcon: {
    minWidth: 'auto',
    color: '#fff',
  },
}));
