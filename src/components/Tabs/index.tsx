import React, {ChangeEvent} from 'react';
import {Tab, Tabs} from '@material-ui/core';

import {useStyles} from './styles';

type Tab = {
  index: string;
  title: string;
}

type Props = {
  tabs: Tab[],
  onTabChange: (event: ChangeEvent<{}>, value: any) => void,
  selectedTab: any,
}

function a11yProps(index: string) {
  return {
    id: `wrapped-tab-${index}`,
    'aria-controls': `wrapped-tabpanel-${index}`,
  };
}

export default function CustomTabs({tabs, onTabChange, selectedTab}: Props) {
  const classes = useStyles();
  return (
      <Tabs
          value={selectedTab}
          onChange={onTabChange}
          classes={{root: classes.tabsRoot}}
          variant="scrollable"
          scrollButtons="auto"
      >
        {tabs.map((tab, key) => {
          return (
            <Tab
              key={key}
              value={tab.index}
              label={tab.title}
              wrapped
              {...a11yProps(tab.index)}
              classes={{root: classes.tabRoot}}
            />
          );
        })}
      </Tabs>
  );
};
