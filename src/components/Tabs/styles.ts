import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  tabsRoot: {
    backgroundColor: theme.palette.action.disabledBackground,
    boxShadow: theme.shadows?.['3'],
  },
  tabRoot: {
    paddingTop: '10px',
    paddingBottom: '10px',
    minWidth: 'auto',
    maxWidth: '180px',
  }
}));
