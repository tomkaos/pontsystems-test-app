import {makeStyles} from '@material-ui/core/styles';
import {drawerWidth} from '../Sidebar';

export const useStyles = makeStyles((theme) => ({
  appWrapper: {
    flexGrow: 1,
    minHeight: '100%',
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.primary,
  },
  content: {
    flexGrow: 1,
    maxWidth: '100%',
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: 200,
    }),
    marginLeft: drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: 200,
    }),
    marginLeft: 0,
  },
  topBar: {
    position: 'fixed',
    zIndex: 1290,
    width: `calc(100% - ${drawerWidth}px)`,
    backgroundColor: theme.palette.background.paper,
    flexGrow: 1,
    maxWidth: '100%',
    left: drawerWidth + 1,
    transition: theme.transitions.create(['left', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: 200,
    }),
  },
  topBarShift: {
    width: '100%',
    left: 0,
    transition: theme.transitions.create(['left', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: 200,
    }),
  },
  topBarInner: {
    padding: theme.spacing(3),
  },
  advert: {
    marginBottom: theme.spacing(2),
  },
}));
