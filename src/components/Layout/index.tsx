import React, {useEffect, useRef, useState} from 'react';
import {Redirect, Route, Switch} from 'react-router';
import * as portals from 'react-reverse-portal';
import {useDispatch, useSelector} from 'react-redux';
import Alert from '@material-ui/lab/Alert';
import {CircularProgress} from '@material-ui/core';
import clsx from 'clsx';

import {routes} from 'config/routes';
import Header from '../Header';
import Sidebar from '../Sidebar';
import Page404 from '../Page404';
import {
  selectSideBar,
  getAdvert,
  selectAdvert,
  selectAdvertLoading,
  selectRouter
} from 'store/ui/uiSlice';
import {useStyles} from './styles';

export const Layout = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useSelector(selectRouter);
  const sideBarOpen = useSelector(selectSideBar);
  const advert = useSelector(selectAdvert);
  const advertLoading = useSelector(selectAdvertLoading);
  const portalNode = portals.createHtmlPortalNode();
  const outPortalEl = useRef(null);
  const topBarEl = useRef(null);
  const contentEl = useRef(null);
  const headerRef = React.createRef();
  const [contentTopMargin, setContentTopMargin] = useState(0);

  function setMargin() {
    // @ts-ignore
    document.querySelector('#topBarContainer').append(outPortalEl.current);
    if (topBarEl.current !== null) {
      // @ts-ignore
      const topBarHeight = topBarEl.current.clientHeight;
      // @ts-ignore
      const headerHeight = headerRef.current.clientHeight;
      setContentTopMargin(topBarHeight - headerHeight + 50);
    }
  }

  useEffect(() => {
    dispatch(getAdvert());
    setMargin();
  // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setMargin();
  // eslint-disable-next-line
  }, [router.location.key, advertLoading]);

  return (
    <div className={classes.appWrapper}>
      <Header ref={headerRef}/>
      <Sidebar />
      <div className={clsx(classes.topBar, {
        [classes.topBarShift]: !sideBarOpen,
      })}>
        <div className={classes.topBarInner} ref={topBarEl}>
          {advertLoading && (
              <div className={classes.advert}>
                <CircularProgress />
              </div>
          )}
          {(advert !== '' && !advertLoading) && (
              <Alert
                  className={classes.advert}
                  severity="info"
                  elevation={6}
                  variant="filled"
              >
                {advert}
              </Alert>
          )}
          <div id="topBarContainer" />
        </div>
      </div>
      <main
          ref={contentEl}
          className={clsx(classes.content, {
            [classes.contentShift]: !sideBarOpen,
          })}
          style={{marginTop: contentTopMargin}}
      >
        <Switch>
          {routes.map((route, index: number) => {
            const Component = route.component;
            return (
              <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  render={props => {
                    if (route.redirectTo) {
                      return <Redirect to={route.redirectTo} key={index}/>;
                    } else {
                      return <Component {...props} portalNode={portalNode} />;
                    }
                  }}
              />
            );
          })}
          <Route component={Page404} />
        </Switch>
      </main>
      <div ref={outPortalEl}>
        <portals.OutPortal node={portalNode} />
      </div>
    </div>
  );
};
