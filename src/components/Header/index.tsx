import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {
  AppBar,
  Toolbar,
  IconButton,
  InputBase,
  Badge,
  Menu,
  MenuItem,
  Typography,
  Switch,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import MenuIconOpen from '@material-ui/icons/MenuOpen';
import SearchIcon from '@material-ui/icons/Search';
import SyncIcon from '@material-ui/icons/Sync';
import HelpIcon from '@material-ui/icons/Help';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircle from '@material-ui/icons/AccountCircle';

import {
  changeTheme,
  selectTheme,
  toggleSideBar,
  selectSideBar
} from 'store/ui/uiSlice';
import {useStyles} from './styles';

const Header = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const themeValue = useSelector(selectTheme);
  const sideBarOpen = useSelector(selectSideBar);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const [switchState, setSwitch] = useState(themeValue === 'dark');

  const handleSwitchChange = () => {
    dispatch(changeTheme(switchState ? 'default' : 'dark'));
    setSwitch(!switchState);
  };

  const handleSidebarToggle = () => {
    dispatch(toggleSideBar());
  };

  const handleProfileMenuOpen = (event: React.BaseSyntheticEvent) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = 'primary-account-menu';
  const renderMenu = (
      <Menu
          anchorEl={anchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          id={menuId}
          keepMounted
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={isMenuOpen}
          onClose={handleMenuClose}
      >
        <MenuItem onClick={handleMenuClose}>Fiók</MenuItem>
        <MenuItem onClick={handleMenuClose}>Kilépés</MenuItem>
      </Menu>
  );

  return (
      <>
        <AppBar position="fixed" className={classes.appBar} ref={ref}>
          <Toolbar>
            <div onClick={handleSidebarToggle}>
              <IconButton
                  edge="start"
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="open drawer"
              >
                {sideBarOpen ? <MenuIconOpen /> : <MenuIcon />}
              </IconButton>
            </div>
            <Switch
                onChange={handleSwitchChange}
                checked={switchState}
            />
            <div className={classes.grow} />
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                  placeholder="Keresés…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  inputProps={{ 'aria-label': 'search' }}
              />
            </div>
            <IconButton aria-label="go to search" color="inherit" component={Link} to="/search">
              <SearchIcon />
            </IconButton>
            <IconButton aria-label="sync" color="inherit" component={Link} to="/sync">
              <SyncIcon />
            </IconButton>
            <IconButton aria-label="help" color="inherit" component={Link} to="/help">
              <HelpIcon />
            </IconButton>
            <IconButton aria-label="mails" color="inherit" component={Link} to="/messages">
              <Badge badgeContent={11} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="notifications" color="inherit" component={Link} to="/notifications">
              <Badge badgeContent={5} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
                edge="end"
                aria-label="user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Typography
                variant="body1"
                color="inherit"
                className={classes.topText}
            >
              Teszt több szerepkör
            </Typography>
          </Toolbar>
        </AppBar>
        {renderMenu}
        <div className={classes.offset} />
      </>
  )
});

export default Header;
