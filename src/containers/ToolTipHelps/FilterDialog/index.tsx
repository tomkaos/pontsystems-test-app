import React, {Dispatch, SetStateAction} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField
} from '@material-ui/core';
import {useSelector} from 'react-redux';

import {selectTooltips} from 'store/adminTooltips/adminTooltipsSlice';
import {useStyles} from '../styles';

type Props = {
  dialogOpen: boolean,
  setDialogOpen: Dispatch<SetStateAction<boolean>>,
  setFilter: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void,
  onSubmit: () => void,
};

export default function FilterDialog({dialogOpen, setDialogOpen, setFilter, onSubmit}: Props) {
  const classes = useStyles();
  const {filter} = useSelector(selectTooltips);
  const foundUrlapNev = filter.filters.find(f => f.name === 'urlapNev');
  const urlapNevFilter = foundUrlapNev ? foundUrlapNev.value : '';
  const foundTab = filter.filters.find(f => f.name === 'tab');
  const tabFilter = foundTab ? foundTab.value : '';
  const foundBlokk = filter.filters.find(f => f.name === 'blokk');
  const blokkFilter = foundBlokk ? foundBlokk.value : '';
  const foundMezo = filter.filters.find(f => f.name === 'mezo');
  const mezoFilter = foundMezo ? foundMezo.value : '';
  const foundTooltip = filter.filters.find(f => f.name === 'tooltip');
  const tooltipFilter = foundTooltip ? foundTooltip.value : '';
  return (
    <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
      <DialogTitle>Szűrés</DialogTitle>
      <DialogContent>
        <form className={classes.formRoot} noValidate autoComplete="off">
          <Grid container spacing={1} className={classes.formRow}>
            <Grid item md={6}>
              <TextField
                  name="urlapNev"
                  size="small"
                  label="Űrlap neve"
                  value={urlapNevFilter}
                  variant="outlined"
                  onChange={setFilter}
              />
            </Grid>
            <Grid item md={6}>
              <TextField
                  name="tab"
                  size="small"
                  label="Tabok"
                  value={tabFilter}
                  variant="outlined"
                  onChange={setFilter}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1} className={classes.formRow}>
            <Grid item md={6}>
              <TextField
                  name="blokk"
                  size="small"
                  label="Blokkok"
                  value={blokkFilter}
                  variant="outlined"
                  onChange={setFilter}
              />
            </Grid>
            <Grid item md={6}>
              <TextField
                  name="mezo"
                  size="small"
                  label="Mezők"
                  value={mezoFilter}
                  variant="outlined"
                  onChange={setFilter}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1} className={classes.formRow}>
            <Grid item md={6}>
              <TextField
                  name="tooltip"
                  size="small"
                  label="Tooltip"
                  value={tooltipFilter}
                  variant="outlined"
                  onChange={setFilter}
              />
            </Grid>
          </Grid>
        </form>
      </DialogContent>
      <DialogActions style={{justifyContent: 'flex-start'}}>
        <Button
            style={{width: '100%'}}
            variant="contained"
            color="primary"
            onClick={onSubmit}
        >Szűrés</Button>
      </DialogActions>
    </Dialog>
  );
}
