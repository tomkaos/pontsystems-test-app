import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  headerWrapper: {
    display: 'flex',
    paddingTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  noMargin: {
    margin: 0,
  },
  headerButtonRoot: {
    minWidth: 'auto',
  },
  headerButtonIcon: {
    margin: 0,
  },
  formRoot: {
    paddingTop: '10px',
    width: '100%',
    '& .MuiTextField-root': {
      width: '100%',
    },
  },
  formRow: {
    marginBottom: theme.spacing(2),
  }
}));

export const useTableStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(4),
  },
  container: {
    //maxHeight: 340,
  },
  tableHeadRoot: {
    backgroundColor: theme.palette.primary.light,
    boxShadow: theme.shadows?.['4'],
  },
  tableHeadCell: {
    color: '#fff',
  },
  tableRowRoot: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}));
