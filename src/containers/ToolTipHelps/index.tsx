import React, {ChangeEvent, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import * as portals from 'react-reverse-portal';
import {
  CircularProgress,
  Button,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  TableContainer,
} from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import {
  getTooltipsData,
  selectTooltips,
  updatePage,
  updatePageSize,
  updateFilter,
} from 'store/adminTooltips/adminTooltipsSlice';
import TablePaginationActions, {TablePaginationSummary} from 'components/TablePagination';
import CustomTooltip from 'components/Tooltip';
import {useStyles, useTableStyles} from './styles';
import FilterDialog from './FilterDialog';

const columns = [
  { id: 'urlapNev', label: 'Űrlap neve' },
  { id: 'tab', label: 'Tab neve' },
  { id: 'blokk', label: 'Blokk neve'},
  { id: 'mezo', label: 'Mező neve'},
];

type Props = {
  portalNode: any,
};

export default function ToolTipHelps({portalNode}: Props) {
  const classes = useStyles();
  const table = useTableStyles();
  const dispatch = useDispatch();
  const {loading, data, filter} = useSelector(selectTooltips);
  const [dialogOpen, setDialogOpen] = React.useState(false);

  const loadData = () => {
    dispatch(getTooltipsData(filter));
  };

  useEffect(() => {
    loadData();
    // eslint-disable-next-line
  },[]);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null, newPage: number) => {
    dispatch(updatePage(newPage));
    console.log('new page: ', newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<any>) => {
    dispatch(updatePageSize(event.target.value));
    console.log('Új sorok száma:', event.target.value);
  };

  const handleClickFilterButton = () => {
    setDialogOpen(!dialogOpen);
  };

  const handleOnClickSaveFilter = () => {
    loadData();
    setDialogOpen(false);
  };

  const setFilter = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const {name, value} = e.target;
    const payload = {
      filterId: name,
      value,
    };
    dispatch(updateFilter(payload));
  }

  return (
    <React.Fragment>
      <portals.InPortal node={portalNode}>
        <React.Fragment>
          <div className={classes.headerWrapper}>
            <h1 className={classes.noMargin}>Help szövegek karbantartása</h1>
            <div className={classes.grow} />
            {!loading && (
                <Button
                    variant="contained"
                    color="primary"
                    classes={{
                      root: classes.headerButtonRoot,
                      startIcon: classes.headerButtonIcon
                    }}
                    onClick={handleClickFilterButton}
                    startIcon={<FilterListIcon />}
                >{''}</Button>
            )}
            <FilterDialog
                dialogOpen={dialogOpen}
                setDialogOpen={setDialogOpen}
                setFilter={setFilter}
                onSubmit={handleOnClickSaveFilter}
            />
          </div>
        </React.Fragment>
      </portals.InPortal>
      <div>
        {loading ? <CircularProgress /> : (
            <React.Fragment>
              <TableContainer className={table.container}>
                <Table aria-label="sticky table">
                  <TableHead classes={{root: table.tableHeadRoot}}>
                    <TableRow>
                      {columns.map(column => (
                          <TableCell
                              key={column.id}
                              align="left"
                              style={{fontWeight: 700}}
                              classes={{root: table.tableHeadCell}}
                          >
                            {column.label}
                          </TableCell>
                      ))}
                      <TableCell key="actions"/>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data.content.map((row, key) => {
                      return (
                        <TableRow hover tabIndex={-1} key={key} classes={{root: table.tableRowRoot}}>
                          {columns.map(column => {
                            // @ts-ignore
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align="left" style={{minWidth: '100px'}}>
                                {value}
                              </TableCell>
                            );
                          })}
                          <TableCell align="right" style={{minWidth: '80px'}}>
                            <CustomTooltip content={`Tooltip: ${row.tooltip ? row.tooltip : 'Nincs adat'}`}>
                              <VisibilityIcon />
                            </CustomTooltip>
                            <CustomTooltip content={`Súgó: ${row.sugo ? row.sugo : 'Nincs adat'}`}>
                              <EditIcon />
                            </CustomTooltip>
                            <CustomTooltip content="Törlés">
                              <DeleteIcon />
                            </CustomTooltip>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              {filter && (
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={data.totalElements}
                    rowsPerPage={filter.size}
                    page={filter.page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    labelRowsPerPage="Sorok száma"
                    ActionsComponent={TablePaginationActions}
                    labelDisplayedRows={TablePaginationSummary}
                />
              )}
            </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
}
