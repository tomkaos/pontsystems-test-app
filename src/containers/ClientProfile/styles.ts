import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  headerWrapper: {
    display: 'flex',
    padding: 20,
    backgroundColor: theme.palette.background.default,
    borderRadius: '3px',
    boxShadow: theme.shadows?.['2'],
    marginBottom: theme.spacing(4),
  },
  noMargin: {
    margin: 0,
  },
  headerButton: {
    marginLeft: theme.spacing(1),
  },
  headerButtonRoot: {
    minWidth: 'auto',
  },
  headerButtonIcon: {
    margin: 0,
  },
}));
