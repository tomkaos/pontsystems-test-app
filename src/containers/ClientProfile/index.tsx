import React, {ChangeEvent, useState} from 'react';
import * as portals from 'react-reverse-portal';
import {Button} from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';
import PollIcon from '@material-ui/icons/Poll';
import PrintIcon from '@material-ui/icons/Print';

import Tabs from 'components/Tabs';
import {TABS} from 'config/constants';
import BasicClientData from './TabPanels/BasicClientData';
import DumpTabPanel from './TabPanels/DumpPanel';
import {useStyles} from './styles';

type Props = {
  portalNode: any,
};

export default function ClientProfile({portalNode}: Props) {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = useState('one');
  const [formEditable, setFormEditable] = React.useState(false);

  const handleChangeTab = (event: ChangeEvent<{}>, newValue: any) => {
    setSelectedTab(newValue);
  };

  const handleChangeFormEditable = () => {
    setFormEditable(!formEditable);
  }

  function renderTabPanel() {
    switch (selectedTab) {
      case 'one':
        return <BasicClientData formEditable={formEditable}/>;
      case 'two':
        return <DumpTabPanel />;
      case 'three':
        return <DumpTabPanel />;
      case 'four':
        return <DumpTabPanel />;
      case 'five':
        return <DumpTabPanel />;
      default:
        return <BasicClientData formEditable={formEditable}/>;
    }
  }

  return (
    <React.Fragment>
      <portals.InPortal node={portalNode}>
        <React.Fragment>
          <div className={classes.headerWrapper}>
            <h1 className={classes.noMargin}>Ügyfél űrlap</h1>
            <div className={classes.grow} />
            <Button
                variant="contained"
                color="primary"
                className={classes.headerButton}
                classes={{
                  root: classes.headerButtonRoot,
                  startIcon: classes.headerButtonIcon
                }}
                onClick={handleChangeFormEditable}
                startIcon={formEditable ? <CancelPresentationIcon/> : <CreateIcon />}
            >{''}</Button>
            <Button
                variant="contained"
                color="primary"
                className={classes.headerButton}
                classes={{
                  root: classes.headerButtonRoot,
                  startIcon: classes.headerButtonIcon
                }}
                startIcon={<PollIcon />}
            >{''}</Button>
            <Button
                variant="contained"
                color="primary"
                className={classes.headerButton}
                classes={{
                  root: classes.headerButtonRoot,
                  startIcon: classes.headerButtonIcon
                }}
                startIcon={<PrintIcon />}
            >{''}</Button>
          </div>
          <Tabs
              selectedTab={selectedTab}
              tabs={TABS}
              onTabChange={handleChangeTab}
          />
        </React.Fragment>
      </portals.InPortal>
      <div>
        {renderTabPanel()}
      </div>
    </React.Fragment>
  );
}
