import React, {ChangeEvent} from 'react';
import {useSelector} from 'react-redux';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  TableContainer,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanel,
  Typography,
  Paper,
} from '@material-ui/core/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

import CustomTooltip from 'components/Tooltip';
import {addressType, selectAddresses} from 'store/client/clientSlice';
import TablePaginationActions, {TablePaginationSummary} from 'components/TablePagination';
import AddressesList from './AddressesList';
import {useStyles, useTableStyles} from './styles';

const columns = [
  { id: 'country', label: 'Ország', minWidth: 100 },
  { id: 'postcode', label: 'Irányítószám', minWidth: 100 },
  { id: 'city', label: 'Város', minWidth: 100 },
];

type Props = {
  formEditable: boolean,
};

export default function BasicClientData({formEditable}: Props) {
  const classes = useStyles();
  const table = useTableStyles();
  const [expanded, setExpanded] = React.useState<any>(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const addresses: addressType[] = useSelector(selectAddresses);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<any>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleChange = (panel: any) => (event: ChangeEvent<{}>, isExpanded: any) => {
    setExpanded(isExpanded ? panel : false);
  };

  const tooltipContent = (
    <React.Fragment>
      <em>Ez egy tooltip szövege</em>
    </React.Fragment>
  );

  return(
    <div className={classes.root}>
      <ExpansionPanel
          classes={{
            root: classes.expansionRoot,
          }}
          expanded={expanded === 'panel1'}
          onChange={handleChange('panel1')}
      >
        <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
            classes={{
              expanded: classes.expansionRootExpanded,
            }}
        >
          <Typography className={classes.heading}>Ügyfél alapadatok</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            Itt jelenleg nincs tartalom.
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
          classes={{
            root: classes.expansionRoot,
          }}
          expanded={expanded === 'panel2'}
          onChange={handleChange('panel2')}
      >
        <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
            classes={{
              expanded: classes.expansionRootExpanded,
            }}
        >
          <Typography className={classes.heading}>Székhely</Typography>
          <div className={classes.grow} />
          <CustomTooltip content={tooltipContent}>
            <HelpOutlineIcon />
          </CustomTooltip>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            Itt jelenleg nincs tartalom.
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
          classes={{
            root: classes.expansionRoot,
          }}
          expanded={expanded === 'panel3'}
          onChange={handleChange('panel3')}
      >
        <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3bh-content"
            id="panel3bh-header"
            classes={{
              expanded: classes.expansionRootExpanded,
            }}
        >
          <Typography className={classes.heading}>Telephely</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{flexDirection: 'column'}}>
          <Paper className={table.root}>
            <TableContainer className={table.container}>
              <Table>
                <TableHead classes={{root: table.tableHeadRoot}}>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell
                        key={column.id}
                        align="left"
                        style={{
                          minWidth: column.minWidth,
                          fontWeight: 700,
                          color: '#fff'
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {addresses.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                    return (
                      <TableRow hover tabIndex={-1} key={row.id} classes={{root: table.tableRowRoot}}>
                        {columns.map(column => {
                          // @ts-ignore
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align="left">
                              {value}
                            </TableCell>
                          )
                        })}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={addresses.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              labelRowsPerPage="Sorok száma"
              ActionsComponent={TablePaginationActions}
              labelDisplayedRows={TablePaginationSummary}
            />
          </Paper>
          <AddressesList
              addresses={addresses.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
              editable={formEditable}
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
};
