import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  root: {
    width: '100%',
  },
  expansionRoot: {
    marginBottom: theme.spacing(4),
  },
  expansionRootExpanded: {
    backgroundColor: theme.palette.primary.light,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightBold,
  },
  formRoot: {
    paddingTop: '10px',
    width: '100%',
    '& .MuiTextField-root': {
      width: '100%',
    },
  },
  formRow: {
    marginBottom: theme.spacing(2),
  }
}));
