import React, {ChangeEvent} from 'react';
import {useDispatch} from 'react-redux';
import {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ExpansionPanel,
  Typography,
  Grid,
  TextField,
  MenuItem,
} from '@material-ui/core/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {addressType, updateAddress} from 'store/client/clientSlice';
import {countries} from 'config/countries';
import {useStyles} from './styles';

type Props = {
  addresses: addressType[],
  editable: boolean,
};

export default function AddressesList({addresses, editable}: Props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState<any>('panel0');
  const dispatch = useDispatch();

  const handleChange = (panel: any) => (event: ChangeEvent<{}>, isExpanded: any) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleTextFieldChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, addressId: number) => {
    const {name, value} = e.target;
    const payload = {
      id: addressId,
      name,
      value,
    };
    dispatch(updateAddress(payload));
  }

  return (
    <React.Fragment>
      {addresses.map((address, key) => {
        const panelName = 'panel' + key;
        const panelControls = panelName + '-content';
        const panelId = panelName + 'header';
        return (
            <ExpansionPanel
                key={address.id}
                classes={{
                  root: classes.expansionRoot,
                }}
                expanded={expanded === panelName}
                onChange={handleChange(panelName)}
            >
              <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls={panelControls}
                  id={panelId}
                  classes={{
                    expanded: classes.expansionRootExpanded,
                  }}
              >
                <Typography className={classes.heading}>Telephely #{key}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <form className={classes.formRoot} noValidate autoComplete="off">
                  <Grid container spacing={1} className={classes.formRow}>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          select
                          required
                          name="country"
                          size="small"
                          label="Ország"
                          value={address.country}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.country === ''}
                          helperText={address.country === '' ? 'A mező kitöltése kötelező' : ''}
                      >
                        {countries.map((option, key) => {
                          return (
                            <MenuItem key={key} value={option.country}>
                              {option.country}
                            </MenuItem>
                          );
                        })}
                      </TextField>
                    </Grid>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          required
                          name="postcode"
                          size="small"
                          label="Irányítószám"
                          value={address.postcode}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.postcode === ''}
                          helperText={address.postcode === '' ? 'A mező kitöltése kötelező' : ''}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={1} className={classes.formRow}>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          required
                          name="city"
                          size="small"
                          label="Város"
                          value={address.city}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.city === ''}
                          helperText={address.city === '' ? 'A mező kitöltése kötelező' : ''}
                      />
                    </Grid>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          required
                          name="street"
                          size="small"
                          label="Közterület neve"
                          value={address.street}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.street === ''}
                          helperText={address.street === '' ? 'A mező kitöltése kötelező' : ''}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={1}>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          required
                          name="street_type"
                          size="small"
                          label="Közterület jellege"
                          value={address.street_type}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.street_type === ''}
                          helperText={address.street_type === '' ? 'A mező kitöltése kötelező' : ''}
                      />
                    </Grid>
                    <Grid item md={6}>
                      <TextField
                          disabled={!editable}
                          required
                          name="house_number"
                          size="small"
                          label="Házszám"
                          value={address.house_number}
                          variant="outlined"
                          onChange={(e) => handleTextFieldChange(e, address.id)}
                          error={address.house_number === ''}
                          helperText={address.house_number === '' ? 'A mező kitöltése kötelező' : ''}
                      />
                    </Grid>
                  </Grid>
                </form>
              </ExpansionPanelDetails>
            </ExpansionPanel>
        );
      })}
    </React.Fragment>
  );
};
