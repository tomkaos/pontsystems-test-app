import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightBold,
  },
  expansionRoot: {
    marginBottom: theme.spacing(4),
  },
  expansionRootExpanded: {
    backgroundColor: theme.palette.info.light,
  },
}));

export const useTableStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(4),
  },
  container: {
    maxHeight: 340,
  },
  tableHeadRoot: {
    backgroundColor: theme.palette.primary.light,
    boxShadow: theme.shadows?.['4'],
  },
  tableRowRoot: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}));
